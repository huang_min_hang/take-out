# take-out
# 说明：

## 静态资源（字体、图标、图片）均来源于网上、项目仅供学习参考

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


### 部分界面展示*截图
##### 主界面:
![avatar](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-e66f9044-b1d1-4a16-9bec-2692a789649d/8eb0b64d-ed53-4109-a413-9913416ae1db.jpg)

##### 购物车:
![avatar](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-e66f9044-b1d1-4a16-9bec-2692a789649d/fc87a2d3-4669-4adf-b4aa-39e0f78f6d6a.jpg)

##### 支付页:
![avatar](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-e66f9044-b1d1-4a16-9bec-2692a789649d/2a1efb95-358a-4602-9539-0d51d7a84300.jpg)

##### 登录注册:
![avatar](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-e66f9044-b1d1-4a16-9bec-2692a789649d/d736c8fa-d4f6-4987-ab07-d72875af9ef9.jpg)

##### 我的:
![avatar](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-e66f9044-b1d1-4a16-9bec-2692a789649d/b803c890-bd8b-48fe-8304-f8087495ed96.jpg)

##### 加载效果:
![avatar](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-e66f9044-b1d1-4a16-9bec-2692a789649d/7072beb6-978e-4b53-89b1-ea5193188547.jpg)