module.exports = {
    devServer:{
        open: true,
        hot: true,
        port: 3030,
        disableHostCheck: true
    }
}