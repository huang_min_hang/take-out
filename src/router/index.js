import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: "/",
    name: "ShopCenter",
    component: () => import("@/views/ShopCenter/index.vue")
  },
  {
    path: "/mine",
    name: "UserCenter",
    component: () => import("@/views/UserCenter/index.vue")
  },
  {
    path: "/cart",
    name: "ShopCart",
    component: () => import("@/views/ShopCart/index.vue")
  },
  {
    path: "/pay",
    name: "PayCart",
    component: () => import("@/views/PayCart/index.vue")
  },
  {
    path: "/test",
    name: "PayCart",
    component: () => import("@/views/Test/index.vue")
  },
  {
    path: "/login",
    name: "Login",
    component: () => import("@/views/Login/index.vue")
  },
  {
    path: "/",
    redirect: "/"
  }
]

const router = new VueRouter({
  routes
})

export default router
