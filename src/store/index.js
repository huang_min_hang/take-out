import Vue from 'vue'
import Vuex from 'vuex'
import ShopCenter from "./modules/ShopCenter"
import Tabar from "./modules/Tabar"

Vue.use(Vuex)

export default new Vuex.Store({
  // state: {
  // },
  // mutations: {
  // },
  // actions: {
  // },
  modules: {
    ShopCenter,
    Tabar
  }
})
