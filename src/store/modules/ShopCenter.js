const state = {
    tipFlag: false,
    tipMsg: "加载中...",
    canClose: true
}

const mutations = {
    CHANGE_TipFLAG: (state, val) => {
        state.tipFlag = val;
    },
    SET_TIPMSG: (state, val) => {
        state.tipMsg = val
    }
}

const actions = {

}

export default {
    state,
    mutations,
    actions,
    namespaced: true
}