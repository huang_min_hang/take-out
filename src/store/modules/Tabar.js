const state = {
    isTabar: true
}

const mutations = {
    CHANGE_TABAR: (state, val) => {
        state.isTabar = val
    }
}

const actions = {

}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}