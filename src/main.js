import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { px2Rem } from "@/config/rem.js"
import "@/config/reset.css"
import "@/config/hover.css"
import Vant, { Toast } from 'vant'
import 'vant/lib/index.css'
import LuckDraw from 'vue-luck-draw'

// 全局注册组件
import Loading from "@/components/Loading/index.vue"

Vue.use(Vant)
Vue.component(Loading.name, Loading)
Vue.use(LuckDraw)
Vue.prototype.$toast = Toast
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App),
  created() {
    px2Rem(16)
  }
}).$mount('#app')
